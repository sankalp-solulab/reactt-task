import React from "react";
import ReactDOM from "react-dom";
import { Route, Switch, BrowserRouter, Redirect } from "react-router-dom";
import { Provider } from "react-redux";
import store from "./store/index"
//CSS FILES
import "./assets/css/bootstrap.min.css";
//LOGIN COMPONENTS
import Login from "./module/auth/login/index.jsx";
//ADMIN COMPONENTS
// import {Dashboard} from "./module/admin/Dashboard/index";
import Dashboard from "./module/admin/Dashboard/Components/dashboard.jsx";
import Setting from "./module/admin/Setting/components/setting.jsx";
import Manageuser from "./module/admin/users/components/users.jsx";
//USER COMPONENTS
// import Feed from "./module/user/component/feed.jsx";
// import Home from "./module/user/component/home.jsx";
// import Profile from "./module/user/component/profile.jsx";
// FOOTER COMPONENTS
import About from "./module/otherpages/About_us/components/aboutus.jsx";
import Contact from "./module/otherpages/Contact_us/components/contactus.jsx";
import Terms from "./module/otherpages/T&C/components/terms.jsx";
// import configureStore from "./store/index";


const App = () => {
  return (
    <Provider store={store}>
    <BrowserRouter>
    {localStorage.getItem("role")==="admin"?(
    <Switch>
        <Route path="/dashboard" component={Dashboard}></Route>
        <Route path="/setting" component={Setting}></Route>
        <Route path="/manageuser" component={Manageuser}></Route>
      </Switch>)
    :null
    }
    
      {/* <Switch>
        <Route path="/feed" component={Feed}></Route>
        <Route path="/home" component={Home}></Route>
        <Route path="/profile" component={Profile}></Route>
      </Switch> */}

      <Switch>
        <Route path="/about" component={About}></Route>
        <Route path="/contact" component={Contact}></Route>
        <Route path="/terms" component={Terms}></Route>
      </Switch>

      <Switch>
        <Route path="/" exact component={Login} />
      </Switch>
    </BrowserRouter>
    </Provider>
  );
};

ReactDOM.render(<App />, document.getElementById("root"));

export default App;
