import { createStore, applyMiddleware, compose } from "redux";
import {rootReducer} from "../Reducers/index";
// const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

// export default function configureStore(initialState) {
//   const store = createStore(
//     initialState,
//     composeEnhancers
//   );

//   return store;
// }

const store = createStore(rootReducer);
export default store;
