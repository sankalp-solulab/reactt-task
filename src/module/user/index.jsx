import React from "react";
import { NavLink } from "react-router-dom";

const User = props => {
  const logoutHandler = () => {
    localStorage.clear();

    // props.history.push("/");
  };
  return (
    <div className="row">
      <div className="col-md-3">
        <NavLink to="#" className="nav-link">
          Home
        </NavLink>
      </div>
      <div className="col-md-3">
        <NavLink to="#" className="nav-link">
          Feed
        </NavLink>
      </div>
      <div className="col-md-3">
        <NavLink to="#" className="nav-link">
          Profile
        </NavLink>
      </div>
      <div className="col-md-3">
        <NavLink className="nav-link" to="/" onClick={() => logoutHandler()}>
          Logout
        </NavLink>
      </div>
    </div>
  );
};

export default User;
