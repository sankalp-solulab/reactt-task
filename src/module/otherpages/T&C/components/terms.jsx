import React from "react";
import Header from "../../../admin/Header/components/header.jsx";
import Footer from "../../footer.jsx";

const Terms = () => {
  return (
    <div>
      <Header />
      Terms & conditions
      <Footer></Footer>
    </div>
  );
};

export default Terms;
