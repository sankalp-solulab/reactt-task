import React from "react";
import Header from "../../../admin/Header/components/header.jsx";

import Footer from "../../footer.jsx";

const About = () => {
  return (
    <div>
      <Header></Header>
      About us
      <Footer></Footer>
    </div>
  );
};

export default About;
