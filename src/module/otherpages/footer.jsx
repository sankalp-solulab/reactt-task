import React, { Fragment } from "react";
import { NavLink, withRouter } from "react-router-dom";

const Footer = props => {
  return (
    <Fragment>
      <div className="row">
        <div className="col-md-3">
          <NavLink to="/about" className="nav-link">
            About us
          </NavLink>
        </div>
        <div className="col-md-3">
          <NavLink to="/contact" className="nav-link">
            Contact us
          </NavLink>
        </div>
        <div className="col-md-3">
          <NavLink to="/terms" className="nav-link">
            Terms & Conditions
          </NavLink>
        </div>
      </div>
      <div className="row">
        <div className="col-md-12 text-center"></div>
      </div>
    </Fragment>
  );
};

export default withRouter(Footer);
