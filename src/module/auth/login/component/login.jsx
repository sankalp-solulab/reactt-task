import React, { Component } from "react";
import { withRouter } from "react-router";

class Login extends Component {
  state = {
    role: "user"
  };
  loginHandler = e => {
    // e.preventDefault();
    console.log(this.state.role);
    localStorage.setItem("role", this.state.role);
    // this.redirectHandler();
  };
  componentWillMount() {
    console.log(this.props);
    const role = localStorage.getItem("role");
    if (role === "admin") {
      this.props.history.push("/dashboard");
    } else if (role === "user") {
      this.props.history.push("/home");
    } else {
      // this.props.history.push("/");
    }
  }

  handleRadio = e => {
    this.setState({ role: e.target.value });
  };
  render() {
    return (
      <div className="row">
        <div className="col-md-4"></div>
        <div className="col-md-4">
          <form className="text-center">
            <legend>Sign in</legend>
            <div className="row">
              <div className="col-md-4">
                <div className="row">
                  <div className="col-md-6">
                    <label>User</label>
                  </div>
                  <div className="col-md-6">
                    <input
                      type="radio"
                      name="optionsRadios"
                      defaultValue="user"
                      defaultChecked
                      onClick={e => this.handleRadio(e)}
                    />
                  </div>
                </div>
              </div>
              <div className="col-md-4">
                <div className="row">
                  <div className="col-md-6">
                    <label>Admin</label>
                  </div>
                  <div className="col-md-6">
                    <input
                      type="radio"
                      name="optionsRadios"
                      defaultValue="admin"
                      onClick={e => this.handleRadio(e)}
                    />
                  </div>
                </div>
              </div>
              <div className="col-md-4">
                <button
                  onClick={e => this.loginHandler(e)}
                  type="submit"
                  className="btn btn-primary"
                >
                  Submit
                </button>
              </div>
            </div>
          </form>
          <div className="col-md-4"></div>
        </div>
      </div>
    );
  }
}

export default withRouter(Login);
