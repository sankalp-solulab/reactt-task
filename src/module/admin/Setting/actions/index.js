import { SETTINGDATA } from "../../../../constants/constants";
export default function settingData(data) {
  // console.log("[In Action dashboardData] ", data);
  return {
    type: SETTINGDATA,
    data
  };
}
