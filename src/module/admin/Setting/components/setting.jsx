import React, { Component } from "react";
import Header from "../../Header/components/header.jsx";
import Footer from "../../../otherpages/footer.jsx";
import { connect } from "react-redux";
import settingData from "../actions/index";
import { SETTINGDATA } from "../../../../constants/constants";

class Setting extends Component {
  componentDidMount() {
    // console.log("[Did Mount]", this.props);
    this.props.settingData(SETTINGDATA);
  }
  render() {
    return (
      <div className="">
        <Header></Header>
        {this.props.data}
        <Footer></Footer>
      </div>
    );
  }
}

const mapStatetoProps = state => ({
  data: state.settingData.data
});

const mapDispatchtoProps = dispatch => ({
  settingData: data => dispatch(settingData(data))
});

export default connect(mapStatetoProps, mapDispatchtoProps)(Setting);
