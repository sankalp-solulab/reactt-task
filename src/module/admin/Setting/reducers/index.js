import { SETTINGDATA } from "../../../../constants/constants";
export const settingData = (state = {}, action) => {
  // console.log("[In Reducer]", action);
  switch (action.type) {
    case SETTINGDATA:
      return {
        ...state,
        data: action.data
      };
    default:
      return state;
  }
};
