import { DASHBOARDDATA } from "../../../../constants/constants";
export default function dashboardData(data) {
  // console.log("[In Action dashboardData] ", data);
  return {
    type: DASHBOARDDATA,
    data
  };
}
