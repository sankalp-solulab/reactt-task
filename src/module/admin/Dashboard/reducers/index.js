import { DASHBOARDDATA } from "../../../../constants/constants";
export const dashboardData = (state = {}, action) => {
  // console.log("[In Reducer]", action);
  switch (action.type) {
    case DASHBOARDDATA:
      return {
        ...state,
        data: action.data
      };
    default:
      return state;
  }
};
