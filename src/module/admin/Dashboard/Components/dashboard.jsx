import React, { Component } from "react";
import Header from "../../Header/components/header.jsx";
import Footer from "../../../otherpages/footer.jsx";
import { DASHBOARDDATA } from "../../../../constants/constants";
import { connect } from "react-redux";
import dashboardData from "../actions/index";

class Dashboard extends Component {
  componentDidMount() {
    // console.log("[Did Mount]", this.props);
    this.props.dashboardData(DASHBOARDDATA);
  }
  render() {
    return (
      <div>
        <Header></Header>
        {this.props.data}
        <Footer></Footer>
      </div>
    );
  }
}

const mapStatetoProps = state => ({
  data: state.dashboardData.data
});

const mapDispatchtoProps = dispatch => ({
  dashboardData: data => dispatch(dashboardData(data))
});

export default connect(mapStatetoProps, mapDispatchtoProps)(Dashboard);
