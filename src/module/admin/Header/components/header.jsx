import React, { Fragment } from "react";
import { NavLink, withRouter } from "react-router-dom";

const Header = props => {
  const logoutHandler = () => {
    localStorage.clear();
    // <Redirect to="/"></Redirect>;
    console.log("logout", props);
    props.history.push("/");
  };
  return (
    <Fragment>
      <div className="row">
        <div className="col-md-3">
          <NavLink to="/dashboard" className="nav-link">
            Dashboard
          </NavLink>
        </div>
        <div className="col-md-3">
          <NavLink to="/setting" className="nav-link">
            Setting
          </NavLink>
        </div>
        <div className="col-md-3">
          <NavLink to="/manageuser" className="nav-link">
            Manage Users
          </NavLink>
        </div>
        <div className="col-md-3">
          <NavLink className="nav-link" to="/" onClick={() => logoutHandler()}>
            Logout
          </NavLink>
        </div>
      </div>
      <div className="row">
        <div className="col-md-12 text-center"></div>
      </div>
    </Fragment>
  );
};

export default withRouter(Header);
