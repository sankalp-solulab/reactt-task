import {dashboardData} from "../module/admin/Dashboard/reducers/index";
import {settingData} from "../module/admin/Setting/reducers/index";
import { combineReducers } from "redux";
export const rootReducer =combineReducers ({dashboardData,settingData})